import restaurant_1 from "./restaurant-1.png";
import restaurant_2 from "./restaurant-2.png";
import restaurant_3 from "./restaurant-3.png";
import timeLogo from "./clock.png";
import broccoliLogo from "./broccoli.png";
import burgerLogo from "./burger.png";
import cakeLogo from "./cake.png";
import meatLogo from "./meat.png";
import pizzaLogo from "./pizza.png";
import sushiLogo from "./sushi.png";

const tags = {
  sushi: {
    name: "Sushi",
    img: sushiLogo,
  },
  burger: {
    name: "Burger",
    img: burgerLogo,
  },
  bbq: {
    name: "Bbq",
    img: meatLogo,
  },
  vegan: {
    name: "Vegan",
    img: broccoliLogo,
  },
  pizza: {
    name: "Pizza",
    img: pizzaLogo,
  },
  desserts: {
    name: "Desserts",
    img: cakeLogo,
  },
};

export const restaurants = [
  {
    name: "Royal Sushi House",
    img: restaurant_1,
    price: 30,
    tags: [tags.sushi],
    categories: ["sushi"],
    time: "30 min",
  },
  {
    name: "Burger Pizza",
    img: restaurant_2,
    price: 30,
    tags: [tags.vegan],
    categories: ["vegan"],
    time: "50 min",
  },
  {
    name: "Vegan House",
    img: restaurant_2,
    price: 50,
    tags: [tags.bbq],
    categories: ["bbq"],
    time: "60",
  },
  {
    name: "Desserts Royal",
    img: restaurant_1,
    price: 60,
    tags: [tags.desserts],
    categories: ["desserts"],
    time: "40",
  },
];
