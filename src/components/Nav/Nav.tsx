import React, { useContext, useEffect, useState } from "react";
import styles from "./Nav.module.css";

import defaultAvatar from "./avatar.png";
import { Link } from "react-router-dom";
import { UserContext } from "../App/App";
import Cart from "../Cart/Cart";
const Nav: React.FC = (): JSX.Element => {
  const user = useContext(UserContext);
  const [avatar, setAvatar] = useState(null);
  useEffect(() => {
    setAvatar(user.image);
  }, [user]);
  return (
    <nav className={styles.nav}>
      <ul className={styles.menu}>
        <li className={styles.menu__item}>
          <a href="/">Restaurants</a>
        </li>
        <li className={styles.menu__item}>
          <a href="/">Deals</a>
        </li>
        <li className={styles.menu__divider} />
        <li className={styles.menu__item}>
          <a href="/">My orders</a>
        </li>
      </ul>
      <div className={styles.user}>
        <Cart />
        <div className={styles.userbox}>
          <Link to="/profile">
            <img
              src={avatar ? URL.createObjectURL(avatar) : defaultAvatar}
              alt="avatar"
              className={styles.user__avatar}
            />
          </Link>
        </div>
      </div>
    </nav>
  );
};

export default Nav;
