import classNames from 'classnames'
import React, { useState } from 'react'
import styles from './FoodMenuItem.module.css'

interface IFoodMenuItem {
  href: string;
  alt: string;
  name: string;
  handleFilter: (item: string) => void;
}

const FoodMenuItem: React.FC<IFoodMenuItem> = ({
  href,
  alt,
  name,
  handleFilter
}): JSX.Element => {
  const [active, setActive] = useState<boolean>(false)
  const handleClick = (name: string): void => {
    handleFilter(name)
    setActive(!active)
  }

  return (
    <div
      onClick={() => handleClick(name)}
      className={classNames(styles.food__menu_item, active && styles.active)}
    >
      <img src={href} alt={alt} />
      <p>{name}</p>
    </div>
  )
}

export default FoodMenuItem
