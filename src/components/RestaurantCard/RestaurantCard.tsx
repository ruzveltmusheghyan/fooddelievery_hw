import React, { useContext } from "react";
import styles from "./RestaurantCard.module.css";
import shoppingBag from "./shoppingbag.png";

import { TagsType } from "../../types/types";
import timeLogo from "./clock.png";
import { HandleCartContext } from "../App/App";
interface TagType {
  name: string;
  img: string;
}
export interface IRestaurantCardProps {
  img: string;
  name: string;
  time: string;
  price: number;
  tags: TagType[];
  featured?: boolean;
  qty?: number;
  categories: string[];
}

const RestaurantCard: React.FC<IRestaurantCardProps> = ({
  img,
  name,
  time,
  price,
  tags,
  featured,
  categories,
}): JSX.Element => {
  const handleCart = useContext(HandleCartContext);
  return (
    <div className={styles.card}>
      <div className={styles.card__image}>
        <img src={img} alt={name} />
      </div>
      <div className={styles.card__info}>
        <div className={styles.card__heading}>
          <h6>{name}</h6>
          <div className={styles.card__shop}>
            <img
              onClick={() =>
                handleCart({
                  img: img,
                  name: name,
                  time: time,
                  price: price,
                  tags: tags,
                  featured: featured,
                  categories: categories,
                })
              }
              src={shoppingBag}
              alt="cart"
            />
            {!featured || <span className={styles.featured}>Featured</span>}
          </div>
        </div>
        <div className={styles.card__price}>
          <img src={timeLogo} alt="time" className={styles.time__logo} />
          <span className={styles.food__time}>{time}</span>
          <span className={styles.circle} />
          <span className={styles.food__price}>{`${price}$`}</span>
        </div>
        <div className={styles.card__tags}>
          {tags.map((tag: TagsType) => {
            return (
              <span key={tag.name} className={styles.food__tag}>
                <img src={tag.img} alt={tag.name} />
                <span className={styles.food_tag__name}>{tag.name}</span>
              </span>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default RestaurantCard;
