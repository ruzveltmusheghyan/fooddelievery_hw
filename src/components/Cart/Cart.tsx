import React, { useContext, useEffect, useRef, useState } from "react";
import { CartContext, RemoveFromCartCotext } from "../App/App";
import { IRestaurantCardProps } from "../RestaurantCard/RestaurantCard";
import styles from "./Cart.module.css";
import classNames from "classnames";
import cartLogo from "./shoppingbag.png";
import { useClickOutside } from "../../hooks/useClickOutside";
const Cart = () => {
  const cartContext = useContext(CartContext);
  const handleRemove = useContext(RemoveFromCartCotext);
  const [cart, setCart] = useState(cartContext);
  const [isOpen, setIsOpen] = useState(false);
  const cartRef = useRef();
  useEffect(() => {
    setCart(cartContext);
  }, [cartContext]);
  useClickOutside(cartRef, () => setIsOpen(false));
  return (
    <div className={styles.cart__container}>
      <img
        onClick={() => (isOpen ? null : setIsOpen(true))}
        src={cartLogo}
        alt="cart"
        className={styles.cart__logo}
      />
      <span>{cart?.length || 0}</span>
      <div
        className={classNames(styles.main_container, {
          [styles.hidden]: !isOpen,
        })}
      >
        <div ref={cartRef} className={styles.container}>
          {cart && cart.length > 0 ? (
            cart?.map((item: IRestaurantCardProps) => {
              return (
                <div
                  className={classNames(styles.flex, styles.justify_between)}
                  key={item.name}
                >
                  <p
                    onClick={() => handleRemove(item.name)}
                    className={styles.delete}
                  >
                    remove
                  </p>
                  <div>{item.name}</div>
                  <div>{`${item.price * item.qty}$`}</div>
                </div>
              );
            })
          ) : (
            <p>No items in cart</p>
          )}
        </div>
      </div>
    </div>
  );
};

export default Cart;
