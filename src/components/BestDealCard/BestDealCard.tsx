import React from 'react'
import styles from './BestDealCard.module.css'

interface IBestDealCard {
  href: string;
  alt: string;
}

const BestDealCard: React.FC<IBestDealCard> = ({ href, alt }): JSX.Element => {
  return (
    <div className={styles.best__deal}>
      <img src={href} alt={alt} />
    </div>
  )
}

export default BestDealCard
