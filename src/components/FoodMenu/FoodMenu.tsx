import React from 'react'
import FoodMenuItem from '../FoodMenuItem/FoodMenuItem'
import styles from './FoodMenu.module.css'
import { FoodMenuConfig } from '../../FoodMenuConfig/FoodMenu'
interface IFoodMenuProps {
  handleFilter: (item: string) => void;
}

const FoodMenu: React.FC<IFoodMenuProps> = ({ handleFilter }): JSX.Element => {
  return (
    <div className={styles.container}>
      {FoodMenuConfig.map((item) => {
        return (
          <FoodMenuItem
            handleFilter={() => handleFilter(item.name)}
            key={item.name}
            href={item.href}
            alt={item.href}
            name={item.name}
          />
        )
      })}
    </div>
  )
}

export default FoodMenu
