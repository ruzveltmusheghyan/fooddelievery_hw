import React from 'react'
import { Link } from 'react-router-dom'
import Nav from '../Nav/Nav'
import Search from '../Search/Search'
import styles from './Header.module.css'
import Logo from './logo.svg'
const Header: React.FC = (): JSX.Element => {
  return (
    <>
      <header>
        <div className={styles.container}>
          <div className={styles.logo__search}>
            <h1>
              <Link to="/">
                <Logo />
              </Link>
            </h1>
            <Search />
          </div>
          <Nav />
        </div>
      </header>
      <div className={styles.header__divider} />
    </>
  )
}

export default Header
