import React from 'react'
import styles from './Search.module.css'
import searchLogo from './search.png'

const Search: React.FC = (): JSX.Element => {
  return (
    <div className={styles.search__container}>
      <input
        type="text"
        placeholder="Search"
        className={styles.header__search}
      />
      <img src={searchLogo} alt="search" className={styles.search__logo} />
    </div>
  )
}

export default Search
