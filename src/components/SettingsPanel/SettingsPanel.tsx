import React from 'react'
import styles from './SettingsPanel.module.css'
import accountLogo from './person.png'
import securityLogo from './shield.png'
import addressLogo from './map.png'
import creditLogo from './credit.png'
import { NavLink, useLocation } from 'react-router-dom'
import classNames from 'classnames'
interface ISettingsPanelCardProps {
  img: string;
  name: string;
  subtext: string;
  href: string;
}

const SettingsPanelCard: React.FC<ISettingsPanelCardProps> = ({
  img,
  name,
  subtext,
  href
}): JSX.Element => {
  const location = useLocation()
  const index = '/profile'
  const to = `${index}${href}`
  const isActive = location.pathname.match(to)
  const isIndex =
    location.pathname === '/profile' || location.pathname === '/profile/'
  console.log(isIndex)
  return (
    <NavLink
      to={`/profile${href}`}
      className={classNames(
        styles.settings__card,
        {
          [styles.active]: isActive
        },
        {
          [styles.active]: href === '/account' && isIndex
        }
      )}
    >
      <img src={img} alt={name} className={styles.card__icon} />
      <div>
        <h6 className={styles.card__heading}>{name}</h6>
        <span className={styles.card__subtext}>{subtext}</span>
      </div>
    </NavLink>
  )
}

const SettingsPanel: React.FC = (): JSX.Element => {
  return (
    <div className={styles.container}>
      <h5 className={styles.heading}>Settings</h5>
      <SettingsPanelCard
        img={accountLogo}
        name="Account"
        subtext="Personal information"
        href="/account"
      />
      <SettingsPanelCard
        img={addressLogo}
        name="Address"
        subtext="Shipping addresses"
        href="/address"
      />
      <SettingsPanelCard
        img={creditLogo}
        name="Payment method"
        subtext="Connected credit cards"
        href="/payment"
      />
      <SettingsPanelCard
        img={securityLogo}
        name="Security"
        subtext="Password,2FA"
        href="/security"
      />
    </div>
  )
}

export default SettingsPanel
