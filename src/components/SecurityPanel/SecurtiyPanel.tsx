import React from 'react'
import styles from './SecurityPanel.module.css'

const SecurityPanel: React.FC = (): JSX.Element => {
  return (
    <div className={styles.container}>
      <div>
        <h5 className={styles.heading}>Security</h5>
      </div>
    </div>
  )
}

export default SecurityPanel
