import React from "react";
import { motion, AnimatePresence } from "framer-motion";

interface IAnimatedPanelProps {
  key?: string;
  children: JSX.Element;
}

const AnimatedPanel: React.FC<IAnimatedPanelProps> = ({
  children,
}): JSX.Element => {
  return (
    <AnimatePresence>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
      >
        {children}
      </motion.div>
    </AnimatePresence>
  );
};

export default AnimatedPanel;
