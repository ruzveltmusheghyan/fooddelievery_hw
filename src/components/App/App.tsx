import React, { useState } from "react";
import Login from "../../pages/Login/Login";
import Home from "../../pages/Home/Home";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Profile from "../../pages/Profile/Profile";
import ProfilePanel from "../ProfilePanel/ProfilePanel";
import AddressPanel from "../AddressPanel/AddressPanel";
import PaymentPanel from "../PaymentPanel/PaymentPanel";
import SecurityPanel from "../SecurityPanel/SecurtiyPanel";
import NotFound from "../../pages/NotFound/Notfound";
import { UserType } from "../../types/types";
import { IRestaurantCardProps } from "../RestaurantCard/RestaurantCard";
export const UserContext = React.createContext<UserType | null>(null);
export const RemoveFromCartCotext = React.createContext(null);
export const ChangeUserContext = React.createContext(null);
export const HandleCartContext = React.createContext(null);
export const CartContext = React.createContext(null);
const App: React.FC = (): JSX.Element => {
  const [isLogin, setIsLogin] = useState<boolean>(false);
  const [user, setUser] = useState<UserType | null>(null);
  const [cart, setCart] = useState<IRestaurantCardProps[] | null>(null);
  const handleCart = (item: IRestaurantCardProps) => {
    if (cart) {
      const inCart = cart.find((el) => (el.name === item.name ? true : false));
      if (inCart) {
        const newCart = cart.map((el) => {
          if (el.name === item.name) {
            return { ...item, qty: el.qty + 1 };
          }
          return el;
        });
        setCart(newCart);
      } else {
        const newCart = [...cart, { ...item, qty: 1 }];
        setCart(newCart);
      }
    } else {
      setCart([{ ...item, qty: 1 }]);
    }
  };
  const removeFromCart = (name: string) => {
    setCart((cart) => cart.filter((el) => el.name !== name));
  };
  return (
    <CartContext.Provider value={cart}>
      <RemoveFromCartCotext.Provider value={removeFromCart}>
        <HandleCartContext.Provider value={handleCart}>
          <UserContext.Provider value={user}>
            <ChangeUserContext.Provider value={setUser}>
              <BrowserRouter>
                <Routes>
                  {isLogin && (
                    <>
                      <Route path="/" element={<Home />} />
                      <Route path="/profile" element={<Profile />}>
                        <Route
                          index
                          element={<ProfilePanel setIsLogin={setIsLogin} />}
                        />
                        <Route
                          path="account"
                          element={<ProfilePanel setIsLogin={setIsLogin} />}
                        />
                        <Route path="address" element={<AddressPanel />} />
                        <Route path="payment" element={<PaymentPanel />} />
                        <Route path="security" element={<SecurityPanel />} />
                      </Route>
                      <Route path="*" element={<NotFound />} />
                    </>
                  )}
                  <Route
                    path="/"
                    element={
                      <Login setIsLogin={setIsLogin} setUser={setUser} />
                    }
                  />
                </Routes>
              </BrowserRouter>
            </ChangeUserContext.Provider>
          </UserContext.Provider>
        </HandleCartContext.Provider>
      </RemoveFromCartCotext.Provider>
    </CartContext.Provider>
  );
};

export default App;
