import React from 'react'
import styles from './PaymentPanel.module.css'

const PaymentPanel: React.FC = (): JSX.Element => {
  return (
    <div className={styles.container}>
      <div>
        <h5 className={styles.heading}>Payment</h5>
      </div>
    </div>
  )
}

export default PaymentPanel
