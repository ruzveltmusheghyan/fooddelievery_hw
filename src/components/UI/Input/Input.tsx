import React from "react";
import styles from "./Input.module.css";
interface IInputProps {
  type: "text" | "password";
  placeholder?: string;
  id: string;
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
  value?: string;
}

const Input: React.FC<IInputProps> = ({
  type,
  placeholder,
  id,
  onChange,
  value,
}): JSX.Element => {
  return (
    <input
      className={styles.input}
      type={type}
      id={id}
      defaultValue={value}
      onChange={onChange}
      placeholder={placeholder}
    />
  );
};

export default Input;
