import classNames from "classnames";
import React from "react";
import styles from "./Button.module.css";
interface IButtonProps {
  variant: "primary" | "secondary" | "borderless";
  value: string;
  onClick?: React.MouseEventHandler<HTMLInputElement>;
}

const Button: React.FC<IButtonProps> = ({
  onClick,
  variant,
  value,
}): JSX.Element => {
  const btnClass = classNames(
    variant === "primary"
      ? styles.btn__primary
      : variant === "borderless"
      ? styles.btn__borderless
      : styles.btn__secondary
  );
  return (
    <input onClick={onClick} type="button" value={value} className={btnClass} />
  );
};

export default Button;
