import React, { SetStateAction, useState } from "react";
import classNames from "classnames";
import styles from "./LoginSection.module.css";
import Logo from "./logo.svg";
import Input from "../UI/Input/Input";
import Button from "../UI/Button/Button";
import users from "../../data/users.json";
import { useNavigate } from "react-router-dom";
import { UserType } from "../../types/types";

interface ILoginSectionProps {
  setIsLogin: React.Dispatch<React.SetStateAction<boolean>>;
  setUser: React.Dispatch<SetStateAction<UserType>>;
}

const LoginSection: React.FC<ILoginSectionProps> = ({
  setIsLogin,
  setUser,
}): JSX.Element => {
  const [login, setLogin] = useState<string>(null);
  const [password, setPassword] = useState<string>(null);
  const [loginError, setLoginError] = useState<boolean>(false);
  const navigate = useNavigate();
  const handleLogin = () => {
    const user = users.find(
      (user) => user.password === password && user.email === login
    );
    if (user) {
      setIsLogin(true);
      setUser(user);
      navigate("/profile", { replace: true });
    } else {
      setLoginError(true);
    }
  };
  return (
    <div className={styles.login_container}>
      <header>
        <div className={styles.container}>
          <a href="#">
            <Logo />
          </a>
        </div>
      </header>
      <div className={classNames(styles.container, styles.login)}>
        <h2 className={styles.login__heading}>Login</h2>
        <p className={styles.login__text}>
          Sign in with your data that you entered during your registration.
        </p>
        {loginError && <p className={styles.error}>Wrong credentials</p>}
        <form action="/" className={styles.login__form}>
          <label htmlFor="email">Email</label>
          <Input
            onChange={(e) => setLogin(e.target.value)}
            id="email"
            type="text"
            placeholder="name@example.com"
          />
          <label htmlFor="password">Password</label>
          <Input
            id="password"
            type="password"
            onChange={(e) => setPassword(e.target.value)}
            placeholder="Min. 8 characters"
          />
          <div className={styles.login__checkbox}>
            <input type="checkbox" name="" id="remember" />
            <label htmlFor="remember">Keep me logged in</label>
          </div>
          <Button
            onClick={() => handleLogin()}
            variant="primary"
            value="Login"
          />
          <Button variant="secondary" value="Forgot password" />
        </form>
        <p className={styles.login__signup}>
          Don’t have an account?{" "}
          <span className={styles.colorized}>Sign up</span>
        </p>
      </div>
    </div>
  );
};

export default LoginSection;
