import React from 'react'
import BestDealCard from '../BestDealCard/BestDealCard'
import styles from './BestDeals.module.css'
import deal_first from './deal_first.png'
import deal_second from './deal_second.png'
const BestDeals: React.FC = (): JSX.Element => {
  return (
    <div className={styles.container}>
      <BestDealCard href={deal_first} alt="milkshake" />
      <BestDealCard href={deal_second} alt="burger" />
    </div>
  )
}

export default BestDeals
