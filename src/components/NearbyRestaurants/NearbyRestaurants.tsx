import React, { useEffect, useState } from "react";
import RestaurantCard from "../RestaurantCard/RestaurantCard";
import styles from "./NearbyRestaurants.module.css";

import { restaurants } from "../../data/data";
import AnimatedPanel from "../HOC/Layout";
import { TagsType } from "../../types/types";

interface INearbyRestaurants {
  filterCards: string[];
}

interface card {
  img: string;
  price: number;
  name: string;
  time: string;
  categories: string[];
  tags: TagsType[];
  featured?: boolean;
}

const NearbyRestaurants: React.FC<INearbyRestaurants> = ({
  filterCards,
}): JSX.Element => {
  const [cards, setCards] = useState<card[]>();
  useEffect(() => {
    setCards(restaurants);
  }, []);
  useEffect(() => {
    if (filterCards.length > 0) {
      const result = restaurants.filter((card) =>
        filterCards.some((tag) => card.categories.includes(tag.toLowerCase()))
      );
      setCards(result);
    } else {
      setCards(restaurants);
    }
  }, [filterCards]);

  return (
    <div className={styles.container}>
      <h3 />
      <div className={styles.card__container}>
        {cards?.map((card) => {
          const { price, tags, time, name, img, featured, categories } = card;
          return (
            <AnimatedPanel key={name}>
              <RestaurantCard
                key={name}
                time={time}
                price={price}
                img={img}
                tags={tags}
                name={name}
                categories={categories}
                featured={featured}
              />
            </AnimatedPanel>
          );
        })}
      </div>
    </div>
  );
};

export default NearbyRestaurants;
