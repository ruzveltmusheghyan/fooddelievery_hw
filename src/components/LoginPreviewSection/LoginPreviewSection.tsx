import React from 'react'
import styles from './LoginPreviewSection.module.css'
import reviews from './reviews.png'
import horizontal from './horizontal.png'
import squared from './squared.png'
import classNames from 'classnames'
const LoginPreviewSection: React.FC = (): JSX.Element => {
  return (
    <div className={styles.preview__container}>
      <div className={styles.review__container}>
        <img src={reviews} alt="reviews" />
        <img src={horizontal} alt="review" className={styles.horizontal} />
        <img src={squared} alt="review" className={styles.squared} />
      </div>
      <h2 className={styles.preview__heading}>Leave reviews for all meals</h2>
      <p className={styles.preview__text}>
        Lorem ipsum dolor sit amet, magna scaevola his ei. Cum te paulo probatus
        molestiae, eirmod assentior eum ne, et omnis antiopam mel.
      </p>
      <div className={styles.circle__container}>
        <div className={styles.circle} />
        <div className={classNames(styles.circle, styles.active)} />
        <div className={styles.circle} />
        <div className={styles.circle} />
      </div>
    </div>
  )
}

export default LoginPreviewSection
