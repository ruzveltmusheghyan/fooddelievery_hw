import React, {
  SetStateAction,
  useContext,
  useEffect,
  useRef,
  useState,
} from "react";
import styles from "./ProfilePanel.module.css";
import avatar from "./main_avatar.png";
import Input from "../UI/Input/Input";
import Button from "../UI/Button/Button";
import { ChangeUserContext, UserContext } from "../App/App";
import { useNavigate } from "react-router-dom";
import AnimatedPanel from "../HOC/Layout";
import { UserType } from "../../types/types";

interface IProfilePanelProps {
  setIsLogin: React.Dispatch<SetStateAction<boolean>>;
}

interface IInputContainer {
  heading: string;
  id: string;
  value: string;
  onChange: React.ChangeEventHandler<HTMLInputElement>;
}

interface ICheckBoxContainerProps {
  name: string;
  id: string;
}
const InputContainer: React.FC<IInputContainer> = ({
  heading,
  value,
  id,
  onChange,
}): JSX.Element => {
  return (
    <div className={styles.input__container}>
      <label htmlFor={id} className={styles.input__heading}>
        {heading}
      </label>
      <Input type="text" onChange={onChange} value={value} id={id} />
    </div>
  );
};

const CheckBoxContainer: React.FC<ICheckBoxContainerProps> = ({
  name,
  id,
}): JSX.Element => {
  return (
    <div className={styles.checkbox__container}>
      <input className={styles.checkbox} type="checkbox" id={id} />
      <label htmlFor={id} className={styles.checkbox__name}>
        {name}
      </label>
    </div>
  );
};

const ProfilePanel: React.FC<IProfilePanelProps> = ({
  setIsLogin,
}): JSX.Element => {
  const user = useContext(UserContext);
  const changeUser = useContext(ChangeUserContext);
  const navigate = useNavigate();
  const [firstName, setFirstName] = useState<string>(user.first_name);
  const [lastName, setLastName] = useState<string>(user.last_name);
  const [phone, setPhone] = useState<string>(user.phone);
  const [email, setEmail] = useState<string>(user.email);
  const [image, setImage] = useState(null);

  const handleLogOut = () => {
    navigate("/", { replace: true });
    setIsLogin(false);
  };
  const handleEdit = () => {
    user.email = email;
    user.first_name = firstName;
    user.phone = phone;
    user.last_name = lastName;
  };

  useEffect(() => {
    user.image = image;
    const newUser = { ...user, image: image };
    changeUser(newUser);
  }, [image]);
  return (
    <AnimatedPanel>
      <div className={styles.container}>
        <div>
          <h5 className={styles.heading}>Account</h5>
          <div className={styles.profile__container}>
            <h5 className={styles.profile__heading}>Personal information</h5>
            <span className={styles.profile__subtext}>Avatar</span>
            <div className={styles.avatar__container}>
              <img
                src={image ? URL.createObjectURL(image) : avatar}
                alt="avatar"
              />
              <label htmlFor="avatar" className={styles.avatar__upload}>
                Change
              </label>
              <input
                id="avatar"
                onChange={(e) => {
                  setImage(e.target.files[0]);
                  e.target.value = "";
                }}
                type="file"
                className={styles.hidden}
              />
              <button
                onClick={() => setImage(null)}
                className={styles.avatar__remove}
              >
                Remove
              </button>
            </div>
            <div>
              <form action="/" className={styles.profile__form}>
                <InputContainer
                  heading="First name"
                  value={user.first_name}
                  id="first_name"
                  onChange={(e) => setFirstName(e.target.value)}
                />
                <InputContainer
                  heading="Last name"
                  value={user.last_name}
                  id="last_name"
                  onChange={(e) => setLastName(e.target.value)}
                />
                <InputContainer
                  heading="Phone"
                  value={user.email}
                  id="email"
                  onChange={(e) => setEmail(e.target.value)}
                />
                <InputContainer
                  heading="Phone"
                  value={user.phone}
                  id="phone"
                  onChange={(e) => setPhone(e.target.value)}
                />
              </form>
            </div>
            <h5 className={styles.profile__heading}>Email notifications</h5>
            <div className={styles.flex}>
              <div className={styles.checkbox_main_container}>
                <CheckBoxContainer name="New deals" id="new_deals" />
                <CheckBoxContainer
                  name="New restaurants"
                  id="new_restaurants"
                />
                <CheckBoxContainer name="Order statuses" id="order_statuses" />
              </div>
              <div className={styles.checkbox_main_container}>
                <CheckBoxContainer name="New deals" id="new_deals" />
                <CheckBoxContainer
                  name="New restaurants"
                  id="new_restaurants"
                />
                <CheckBoxContainer name="Order statuses" id="order_statuses" />
              </div>
            </div>
            <div className={styles.action__container}>
              <div>
                <button
                  onClick={() => handleLogOut()}
                  className={styles.logout__btn}
                >
                  Log out
                </button>
              </div>
              <div className={styles.submit__container}>
                <Button variant="secondary" value="Discard changes" />
                <Button
                  variant="primary"
                  onClick={() => handleEdit()}
                  value="Save changes"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </AnimatedPanel>
  );
};

export default ProfilePanel;
