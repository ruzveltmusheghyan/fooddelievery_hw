import React from 'react'
import { Outlet } from 'react-router-dom'
import Header from '../../components/Header/Header'
import ProfilePanel from '../../components/ProfilePanel/ProfilePanel'
import SettingsPanel from '../../components/SettingsPanel/SettingsPanel'
import styles from './Profile.module.css'
const Profile: React.FC = (): JSX.Element => {
  return (
    <>
      <Header />
      <main>
        <div className={styles.container}>
          <SettingsPanel />
          <Outlet />
        </div>
      </main>
    </>
  )
}

export default Profile
