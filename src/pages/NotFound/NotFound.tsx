import React from "react";
import Header from "../../components/Header/Header";
import styles from "./NotFound.module.css";

const NotFound: React.FC = (): JSX.Element => {
  return (
    <>
      <Header />
      <div className={styles.container}>Page not found</div>
    </>
  );
};

export default NotFound;
