import React, { useCallback, useContext, useState } from "react";
import { UserContext } from "../../components/App/App";
import BestDeals from "../../components/BestDeals/BestDeals";
import FoodMenu from "../../components/FoodMenu/FoodMenu";
import Header from "../../components/Header/Header";
import NearbyRestaurants from "../../components/NearbyRestaurants/NearbyRestaurants";

const Home: React.FC = (): JSX.Element => {
  const [restaurants, setRestaurants] = useState([]);
  const handleFilter = useCallback((item: string) => {
    setRestaurants((restaurants) => {
      if (restaurants.includes(item)) {
        return restaurants.filter((restaurant) => restaurant !== item);
      }

      return [...restaurants, item];
    });
  }, []);
  const user = useContext(UserContext);

  return (
    <>
      <Header />
      <BestDeals />
      <FoodMenu handleFilter={handleFilter} />
      <NearbyRestaurants filterCards={restaurants} />
    </>
  );
};

export default Home;
