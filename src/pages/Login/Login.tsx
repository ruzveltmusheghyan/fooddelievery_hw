import classNames from "classnames";
import React from "react";
import LoginPreviewSection from "../../components/LoginPreviewSection/LoginPreviewSection";
import LoginSection from "../../components/LoginSection/LoginSection";
import { UserType } from "../../types/types";
import styles from "./Login.module.css";

interface ILoginProps {
  setIsLogin: React.Dispatch<React.SetStateAction<boolean>>;
  setUser: React.Dispatch<React.SetStateAction<UserType>>;
}

const Login: React.FC<ILoginProps> = ({ setIsLogin, setUser }): JSX.Element => {
  return (
    <div className={classNames(styles.login_page_container)}>
      <LoginSection setUser={setUser} setIsLogin={setIsLogin} />
      <LoginPreviewSection />
    </div>
  );
};

export default Login;
