export type UserType = {
  email: string;
  first_name: string;
  last_name: string;
  phone: string;
  password: string;
  image?: string;
};

export type TagsType = {
  name: string;
  img: string;
};
