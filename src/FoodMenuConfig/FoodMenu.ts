import broccoliItem from './broccoli.png'
import burgerItem from './burger.png'
import cakeItem from './cake.png'
import meatItem from './meat.png'
import pizzaItem from './pizza.png'
import sushiItem from './sushi.png'

export const FoodMenuConfig = [
  {
    href: pizzaItem,
    name: 'Pizza'
  },
  {
    href: burgerItem,
    name: 'Burger'
  },
  {
    href: meatItem,
    name: 'BBQ'
  },
  {
    href: sushiItem,
    name: 'Sushi'
  },
  {
    href: broccoliItem,
    name: 'Vegan'
  },
  {
    href: cakeItem,
    name: 'Desserts'
  }
]
